// Створити HTML тег на сторінці можна за допомогою createElement() або Element.insertAdjacentHTML();

// Перший параметр функції insertAdjacentHTML означає позицію на яку ми хочемо поставити цей елемент (він вказується другим параметром)
// Параметри бувають beforeBegin (відразу перед батьківським елементом) afterBegin (напочатку батьківського елемента)
// beforeEnd(перед закриваючим тегом батьківського елемента) afterEnd(відразу після закриваючого тега батьківського елемента)

// Видалити елемент зі сторінки можна методом .remove()



//Створити функцію, яка прийматиме на вхід масив і опціональний другий
// аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
//кожен із елементів масиву вивести на сторінку у вигляді пункту списку;


let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 25];

function createList (arr, parent = document.body) {
    let crUl = document.createElement('ul');
    parent.prepend(crUl);
    let list = document.querySelector('ul');
    arr.forEach((elem) => {
        let crLi = document.createElement('li');
        list.append(crLi);
        crLi.innerHTML = elem;
    })
}

createList(arr);
