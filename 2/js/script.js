// 1) Number, string, boolean, object, null, undefined, symbol, bigint
// 2) === строга рівність, у ній не враховується тип даних при порівнянні (true === 'true' / false)
// == не строга рівність,тут не враховується тип даних (true == 'true' / true)
// 3) це символ, який робить "щось" (каталізатор). нариклад "=" - присвоює значення змінній,
// "==" та "===" - перевіряють на рівність, "<" ">" ">=" "<=" - ммм, теж перевіряють рівність!?)... і т. д.


let firstName = prompt('Enter your name?');
let age = +prompt('Enter your age');

while(!age || firstName === '' || !firstName) {
    firstName = prompt('Enter your name again, please!', firstName);
    age = +prompt('Enter your age again, please!', age);
}

if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
    let answer = confirm('Are you sure you want to continue?');
    if (answer) {
        alert('Welcome ' + firstName);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert('Welcome ' + firstName);
}
