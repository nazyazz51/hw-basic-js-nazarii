// Метод об'єкту - це функція яка працює з цим об'єктом.

// Значення властивості об'єкта може мати будь-який тип даних, включаючи об'єкти.

// Називається посилальним, бо змінні у яких він зберігається є посиланням на об'єкт, а не являються самими об'єктами

function createNewUser (firstName, lastName) {
    let newUser = {
        firstName,
        lastName,
        getLogin() {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase()
        }
    };
    return newUser;
} 

let getLogValue = createNewUser(prompt('Enter first name'), prompt('Enter last name')).getLogin();

console.log(getLogValue);

//or

//console.log(createNewUser(prompt('Enter first name'), prompt('Enter last name')).getLogin());