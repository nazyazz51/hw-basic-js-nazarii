const mainUl = document.getElementById('our-services-sections');
        
mainUl.addEventListener('click', (event) => {
    document.querySelectorAll('.tabs-title').forEach((el) => el.classList.remove('active'));
    event.target.classList.add('active');
    
    document.querySelectorAll('.our-services-content > ul').forEach((el) => el.hidden = true);
    document.querySelectorAll('.our-services-content > ul').forEach((el) => el.classList.remove('visible'));


    let myActiveUl = document.querySelector('.' + event.target.dataset.class);
    myActiveUl.hidden = false;
    myActiveUl.classList.add('visible');

});

let isLoadMorePressed=false;

const ourAmazingImgs= document.querySelectorAll('.amazing-first-section img');
const myBtn = document.querySelector('.amazing-button');

myBtn.addEventListener('click', () =>{
    isLoadMorePressed = true;
    ourAmazingImgs.forEach((elem) => {
        elem.classList.remove('dnone');
    })

    myBtn.style.display = 'none';
})

const mainUlCategory = document.getElementById('mainUlCategory');

mainUlCategory.addEventListener('click', (event) => {
    document.querySelectorAll('.tabs-title').forEach((el) => el.classList.remove('active'));
    event.target.classList.add('active');
    
    ourAmazingImgs.forEach((el, idx) => {
        if(event.target.dataset.catecory === 'all-category') {
            if (isLoadMorePressed || idx < 16) {
                el.classList.remove('dnone')
            } else {
                el.classList.add('dnone')
            }

            if (!isLoadMorePressed) {
                myBtn.classList.remove('dnone')
            }

            return;
        }

        myBtn.classList.add('dnone')


        if(el.dataset.catecory === event.target.dataset.catecory) {
            el.classList.remove('dnone')
        } else {
            el.classList.add('dnone')
        }
    })

});


let slideActive = 0;

const carousel = document.querySelector('.carousel');
const carouselPagination = document.querySelectorAll('.carousel-pagination li[data-slide]');

carouselPagination.forEach(el => {
    el.addEventListener('click', (event) => {
        slideActive = parseInt(event.target.parentElement.dataset.slide, 10);

        slide();
    });
});
const arrowBack =document.querySelector('.carousel-pagination .arrow-back');
const arrowForward =document.querySelector('.carousel-pagination .arrow-forward');

arrowBack.addEventListener('click', (ev) => {
    if(slideActive > 0) {
        slideActive = --slideActive;
    }

    if(slideActive === 0) {
        arrowBack.classList.add('disabled');
    }

    slide();
})


arrowForward.addEventListener('click', () => {
    if(slideActive < carouselPagination.length - 1) {
        slideActive = ++slideActive;
    }

    if(slideActive === carouselPagination.length - 1) {
        arrowForward.classList.add('disabled');
    }

    slide();
})
function slide() {
    carousel.style.left = `-${1160 * slideActive}px`; 

    carouselPagination.forEach((element, idx) => {
        if(idx === slideActive) {
            element.classList.add('active');
        } else {
            element.classList.remove('active');
        }
    });
}