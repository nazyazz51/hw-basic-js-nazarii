//1) var, let, const
//2) confirm повертає від користувача тільки true або false, a prompt пвертає те що користувач запише в поле для вводу тексту (поверне типом string), або null (у разі натискання 'Cansel').
//3) '3' * 2 - неявне перетворення відбудеься з трійкою, воно неявне, тому що перетворення відбувається завдяки оператору "*". Було б явне якщо б записали Number('3')*2.

//Practise

//1)
const name = 'Nazarii';
const admin = name;
console.log(admin);

//2)
const days = 7;
let sec = days * 24 * 60 * 60;
console.log(sec);

//3
const age = prompt('How old are you?');
console.log(age);
