// Опишіть своїми словами що таке Document Object Model (DOM)
// Це спосіб взаємодіяти з елементами на сторінці.


//Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML - через цю властивість ми можемо працювати з тегами (додавати, видаляти, змінювати), змінювати структуру
// HTML коду. а innerText працює тільки з текстом, текстовим форматом.


// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Можна звернутися взагалом двома способами 1 - querySelector 2 - document.getElement(s).By*
// Який з них кращий - не знаю. Універсальніший - querySelector, але він довше обробляє
// інформацію ніж document.getElement('s).By*




//Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.body.querySelectorAll('p');
for (let paragraph of paragraphs) {
    paragraph.style.backgroundColor = '#ff0000';
    paragraph.style.opacity = '1';
}


// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.getElementById('optionsList');
console.log(optionsList);

console.log(optionsList.parentElement);
console.dir(optionsList);
let listNodes = optionsList.childNodes;
for (let chNode of listNodes) {
    console.log(chNode.nodeName);
    console.log(chNode.nodeType);
}




// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

// Класу з назвою testParagraph - немає. Але є id.

let newParagraph = document.getElementById('testParagraph');
newParagraph.innerHTML = 'This is a paragraph';


//Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

let elems = document.querySelector('.main-header').children;

let elems2 = document.querySelectorAll('.main-header>div')
console.log(elems2);

for(elem of elems2) {
    elem.classList.add('nav-item');
}




//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let elemsSectionTitle = document.querySelectorAll('.section-title');
for(elem of elemsSectionTitle){
    elem.classList.remove('section-title');
}
